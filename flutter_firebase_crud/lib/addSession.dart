import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase_crud/tb_list.dart';

class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);

  @override
  State<AddSession> createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  //Enabling textinput to connect with firebase
  @override
  Widget build(BuildContext context) {
    TextEditingController smearsentController = TextEditingController();
    TextEditingController tbController = TextEditingController();
    TextEditingController monthController = TextEditingController();
    

    Future _addSession() {
      final smear = smearsentController.text;
      final tb = tbController.text;
      final month = monthController.text;

      final ref = FirebaseFirestore.instance.collection("TB Cases").doc();

      return ref
          .set({"Smear_Sent": smear, "TB_Cases": tb, "Month": month, "doc_id": ref.id})
          .then((value) => {
            smearsentController.text = "",
            tbController.text = "",
            monthController.text = "",

          }
          )
          .catchError((onError) => log(onError));
    }

    return Container(
      width: 800.0,
      alignment: Alignment.center,
      child: Column(
         
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
               Padding(
                padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                child: TextField(
                  controller: smearsentController,
                  decoration: InputDecoration(
                    border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular((20))),
                    hintText: "how many sputum sent?",
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: tbController,
                  decoration: InputDecoration(
                    border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular((20))),
                    hintText: "how many tb positive tb cases?",
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: monthController,
                  decoration: InputDecoration(
                    border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular((20))),
                    hintText: "month",
                  ),
                ),
              ),
             
              ElevatedButton(
                  onPressed: () {
                    _addSession();
                  },
                  child: Text("Save"))
            ],
            
          ),
          TBList()
        ],
        
      ),
    );
  }
}
