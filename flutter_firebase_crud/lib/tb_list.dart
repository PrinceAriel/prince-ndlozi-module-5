import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';

class TBList extends StatefulWidget {
  const TBList({Key? key}) : super(key: key);

  @override
  State<TBList> createState() => _TBListState();
}

class _TBListState extends State<TBList> {
  final Stream<QuerySnapshot> _mySession =
      FirebaseFirestore.instance.collection("TB Cases").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _smearsentFieldCtrlr = TextEditingController();
    TextEditingController _tbFieldCtrlr = TextEditingController();
    TextEditingController _monthFieldCtrlr = TextEditingController();

    
    //Delete method
    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("TB Cases")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }//end delete method

    //update method
    void _update(data){
      var collection = FirebaseFirestore.instance.collection("TB Cases");
      
      _smearsentFieldCtrlr.text = data["Smear_Sent"];
      _tbFieldCtrlr.text = data["TB_Cases"];
      _monthFieldCtrlr.text = data["Month"];

      showDialog(
       context: context,
       builder: (_) => AlertDialog(
        title: Text("Update"),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
             TextField(
            controller: _smearsentFieldCtrlr,
          ),
          TextField(
            controller: _tbFieldCtrlr,
          ),
          TextField(
            controller: _monthFieldCtrlr,
          ),

          TextButton(onPressed: (){
            collection.doc(data["doc_id"]).
            update({
              "Smear_Sent": _smearsentFieldCtrlr.text,
              "TB_Cases": _tbFieldCtrlr.text,
              "Month": _monthFieldCtrlr.text,
            });
            Navigator.pop(context);
          }, 
          child: Text("Update")),
        ]),
       )
       );
    }//end update method

    return StreamBuilder(
      stream: _mySession,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                height: (MediaQuery.of(context).size.height),
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  children: snapshot.data!.docs
                      .map((DocumentSnapshot documentSnapshot) {
                    Map<String, dynamic> data =
                        documentSnapshot.data()! as Map<String, dynamic>;
                    return Column(
                      children: [
                        Card(
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(data['Month']),
                                subtitle: Text("Positive Cases is: " + data['TB_Cases']),
                                 
                              ),
                              ButtonTheme(
                                  child: ButtonBar(
                                children: [
                                  OutlinedButton.icon(
                                    onPressed: () {
                                      _update(data);
                                    },
                                    icon: Icon(Icons.edit),
                                    label: Text("edit"),
                                  )
                                ],
                              )),
                              ButtonTheme(
                                  child: ButtonBar(
                                children: [
                                  OutlinedButton.icon(
                                    onPressed: () {
                                      _delete(data["doc_id"]);
                                    },
                                    icon: Icon(Icons.remove),
                                    label: Text("remove"),
                                  )
                                ],
                              ))
                            ],
                          ),
                        ),
                      ],
                    );
                  }).toList(),
                ),
              ))
            ],
          );
        } else {
          return Text("Theres no data!");
        }
      },
    );
  }
}
