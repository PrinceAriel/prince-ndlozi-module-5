import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'addSession.dart';

//Initialize app to firebase code below
Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyDK0S9sHHK8HDepsfnBoiTujtmuzINltFY",
          authDomain: "flutter-firebase-crud-1b32f.firebaseapp.com",
          projectId: "flutter-firebase-crud-1b32f",
          storageBucket: "flutter-firebase-crud-1b32f.appspot.com",
          messagingSenderId: "65101453062",
          appId: "1:65101453062:web:4b7e2128d779afb0fc7087",
          measurementId: "G-JX535BHMDB"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Crud Firebase',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: const MyHomePage(title: 'Crud FireBase'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
   

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
       title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[AddSession()],
        
    )
    ));
  }
}
